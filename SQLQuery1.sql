create database Video

use Video

create table Customer(
	CustomerId int identity(1,1) primary key,
	CustomerName varchar(255) Not Null,
	CustomerAddress varchar(255) Not Null,
	CustomerPhone Varchar(255) Not Null,
	CreatedAt datetimeoffset Default GetDate(),
)

Create table MemberShip(
	MemberId int identity(1,1) primary key,
	harga int not null,
	CustomerId int foreign key references Customer(CustomerId),	
	CreatedAt datetimeoffset Default GetDate(),
)

create table CustomerPremium(
	CustomerPremiumId int identity(1,1) primary key,
	CustomerName varchar(255) Not Null,
	CustomerAddress varchar(255) Not Null,
	CustomerPhone Varchar(255) Not Null,
	MemberId int foreign key references MemberShip(MemberId),
)

create table Report(
	ReportId int identity(1,1) primary key,
	MemberId int foreign key references MemberShip(MemberId),
)

create table Suspendd(
	ReportId int foreign key references Report(ReportId)
)

create table Adminn(
	AdminId int identity(1,1) primary key,
	AdminName varchar(255) not null,
	AdminAddress varchar(255) Not Null,
	AdminPhone Varchar(255) Not Null
)

create table Video(
	VideoId int identity(1,1) primary key,
	VideoName varchar(255) not null,
	videoGenre Varchar(255) not null,
	CustomerId int foreign key references Customer(CustomerId),
)

create table VideoCategory(
	VideoCategoryId int identity(1,1) primary key,
	VideoName varchar(255) not null,
	videoGenre Varchar(255) not null,
	AllVideo varchar(255) not null,
	CustomerPremiumId int foreign key references CustomerPremium(CustomerPremiumId),
)

create table Ads(
	AdsId int identity(1,1) primary key,
	AdsBrand Varchar(255) not null,
)

create table deals(
	dealsId int identity(1,1) primary key,
	AdsId int foreign key references Ads(AdsId),
	VideoId int foreign key references Video(VideoId)
)

create table DetailNews(
	DetailNewsId int identity(1,1) primary key,
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin',
)

create table News(
	NewsId int identity(1,1) primary key,
	CustomerId int foreign key references Customer(CustomerId),
	CustomerPremiumId int foreign key references CustomerPremium(CustomerPremiumId),
	AdminId int foreign key references Adminn(AdminId),
	DetailNewsId int foreign key references DetailNews(DetailNewsId),
	UpdateAt datetimeoffset Default GetDate(),
	UpdateBy varchar(255) default 'Admin',
)

create table TopicCategory(
	TopicCategotyId int identity(1,1) primary key,
	TopicDescription varchar(255) not null
)

create table Forum(
	ForumId int identity(1,1) primary key,
	ForumTopic varchar(255) not null,
	CustomerId int foreign key references Customer(CustomerId),
	CustomerPremiumId int foreign key references CustomerPremium(CustomerPremiumId),
)

create table Topic(
	TopicId int identity(1,1) primary key,
	ForumId int foreign key references Forum(ForumId),
	TopicCategotyId int foreign key references TopicCategory(TopicCategotyId)
)



Drop table Topic,Forum,TopicCategory,News,DetailNews,Ads,VideoCategory,Video,Adminn,Suspendd,Report,CustomerPremium,MemberShip,Customer,deals
